/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ep2;

public class VeiculoNormal extends Veiculo {
    private String tipoCombustivel;
    private double precoCombustivel;
    private double rendimentoInicial;
    private double perdaRendimento;
    private double rendimentoFinal;
  
    public String getTipoCombustivel() {
        return tipoCombustivel;
    }

    public void setTipoCombustivel(String tipoCombustivel) {
        this.tipoCombustivel = tipoCombustivel;
    }

    public double getPrecoCombustivel() {
        return precoCombustivel;
    }

    public void setPrecoCombustivel(double precoCombustivel) {
        this.precoCombustivel = precoCombustivel;
    }

    public double getRendimentoInicial() {
        return rendimentoInicial;
    }

    public void setRendimentoInicial(double rendimentoInicial) {
        this.rendimentoInicial = rendimentoInicial;
    }

    public double getPerdaRendimento() {
        return perdaRendimento;
    }

    public void setPerdaRendimento(double perdaRendimento) {
        this.perdaRendimento = perdaRendimento;
    }

    public double getRendimentoFinal() {
        return rendimentoFinal;
    }

    public void setRendimentoFinal(double rendimentoFinal) {
        this.rendimentoFinal = rendimentoFinal;
    }
    
    public void CalculaRendimentoFinal(double peso){
        setRendimentoFinal(getRendimentoInicial() - (peso * getPerdaRendimento()));
    }
    
    @Override
    public void CalculaCusto(double pesoCarga, double distancia){
        double custo;
        custo = (distancia / getRendimentoFinal()) * getPrecoCombustivel();
        
        setCustoViagem(custo);
    }
    
    @Override
    public void CalculaTempo(double distancia){
        double tempo;
        tempo = distancia / getVelMedia();
        
        setTempoViagem(tempo);
    }
}
