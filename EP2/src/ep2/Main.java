package ep2;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
    
    public static void main(String[] args) {
        
        Arquivo arquivo = new Arquivo();
        
        try {
            arquivo.GetArquivoVeiculos();
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
            arquivo.GetArquivoMargemDeLucro();
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }   
        
        try {
            arquivo.GetArquivoInfoViagens();
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
           
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(InterfaceMainMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(InterfaceMainMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(InterfaceMainMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(InterfaceMainMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }   
           
        InterfaceMainMenu mainMenu = new InterfaceMainMenu();
        mainMenu.setVisible(true);
          
           
      /*  
        Carreta carreta1 = new Carreta();
        carreta1.Status();
        
        System.out.println("---------------------");
        
        Moto moto1 = new Moto();
        moto1.Status();
        
        System.out.println("---------------------");
        
        Carro carro1 = new Carro();
        carro1.Status();
        
        System.out.println("---------------------");
        
        Van van1 = new Van();
        van1.Status();
      */        
    }
 } 
